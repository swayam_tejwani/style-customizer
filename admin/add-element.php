<style>
.tag-element-page label span{ font-size:12px; font-style:italic;}
</style>
<div class="wrap tag-element-page">
<h2>Add Tag Element</h2>
<br>
<form method="post" id="add_element_form">
<label for="add_tag">Add Tags <span>(for eg. p, span, h1, h2 etc)</span></label>
<input id="add_tag" name="add_tag" type="text">
<input type="button" id="btn_add_tag" class="button button-primary button-large" value="Add Tag">
</form>
</div>