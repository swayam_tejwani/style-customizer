<div class="wrap">
<h2>Style Customizer</h2>
<?php
if(isset($_POST) && ($_POST['style_customizer'] == 1)){
	// process form data
	$select_tag = $_POST['select_tag'];
	$select_type = $_POST['select_type'];
	$tag_data = $_POST['fill_css'];

	if(sc_update_tag_data( $select_tag,$select_type,$tag_data )){
	?><div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"> 
<p><strong>Settings saved.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div><?php	
	}
}
?>
<form method="post" action="">
<input type="hidden" name="style_customizer" value="1">
<table class="form-table">
<tbody>
	<tr>
		<th scope="row"><label for="select_tag">Choose Tag Element</label></th>
		<td>
			<select id="select_tag" name="select_tag">
			<option value="">Select</option>
			<?php
			$all_tags = sc_list_tag_elements();
			if( $all_tags ){
				foreach( $all_tags as $tags ){
					?>
					<option value="<?php echo $tags->id; ?>"><?php echo $tags->tag; ?></option>
					<?php
				}
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<th scope="row"><label for="select_type">Select CSS Type</label></th>
		<td>
		<input type="radio" name="select_type" value="inline" class="select_type sc_inline">Inline
		<input type="radio" name="select_type" value="internal" class="select_type sc_internal">Internal
		</td>
	</tr>
	<tr>
		<th scope="row"><label for="fill_css">Enter CSS Properties</label></th>
		<td>
		<textarea name="fill_css" id="fill_css" rows="10" cols="50"></textarea>
		<!--<p><span>For inline mode, input should be "font-size":"16px","color":"red" etc</span></p>-->
		</td>
	</tr>
</tbody>
</table>
<p class="submit">
<input type="submit" name="submit_btn" id="submit_btn" value="Submit" class="button button-primary">
</p>
</form>
<hr style="height:30px;">
<h2>External CSS File Upload</h2>
<?php
if(isset($_POST['submit_file'])){
	$upload_dir = wp_upload_dir();
	$target_dir = $upload_dir['basedir'].'/sc-uploads/';
	
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$errors = array();
	$uploadOk = 1;
	$FileType = pathinfo($target_file,PATHINFO_EXTENSION);
	if($FileType != 'css'){
		$errors[] = "File Extension should be .css";
	}
	// Check if file already exists
	if (file_exists($target_file)) {
		$errors[] = "Sorry, File with the same name already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 500000) {
		$errors[] = "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	if($uploadOk == 0){
		?>
		<style>
		.upload-error{
			color:red;
			font-weight:bold;
		}
		</style>
		<?php
		if(!empty($errors)){
			foreach($errors as $error){
				echo '<p class="upload-error">'.$error.'</p>';
			}
		}
	}else{
		if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
		$uploadedfile = $_FILES['fileToUpload'];
		$upload_overrides = array( 'test_form' => false );
		add_filter('upload_dir', 'sc_upload_dir');
		$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
		remove_filter('upload_dir', 'sc_upload_dir');

		if ( $movefile ) {
			$style = "color:green";
			echo "<b style=$style>File is successfully uploaded.\n</b>";
		} else {
			
		}
	}
}
?>
<form action="" method="post" enctype="multipart/form-data">
Select image to upload:
<input type="file" name="fileToUpload" id="fileToUpload">
<input class="button button-primary" type="submit" value="Upload CSS" name="submit_file">
</form>
<?php
	$upload_dir = wp_upload_dir();
	$directory = $upload_dir['basedir'].'/sc-uploads/';

    if (! is_dir($directory)) {
        exit();
    }

    $files = array();

    foreach (scandir($directory) as $file) {
        if ('.' === $file) continue;
        if ('..' === $file) continue;

        $files[] = $file;
    }

	if(!empty($files)){
		echo "<br>";
		echo '<div style="font-size: 14px;font-weight: bold;">Uploaded CSS Files List</div>';
		foreach($files as $file){
			echo "<div>";
			//$filename = explode(".css",$file);
			echo '<p>'.$file.' <a class="del_css" id='.$file.' href="javascript:void(0);">Delete</a></p>';
			echo "</div>";
		}
	}
?>
</div>