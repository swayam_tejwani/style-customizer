<?php
/*
Plugin Name: Style Customization
Plugin URI: http://google.com
Description: This plugin allows to add global style property to the page elements.
Author: Swayam Tejwani
Version: 1.0
*/


//  Create custom table on plugin activation for tags and their style data.
function sc_install_setup(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'tag_elements';
	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		tag varchar(55),
		style_type varchar(55),
		style_data varchar(255),
		PRIMARY KEY (id)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}
register_activation_hook( __FILE__, 'sc_install_setup' );
	
/*
* Register a custom menu page for the plugin settings.
*/
function sc_register_main_page() {
	global $sc_menu_page;
	global $sc_submenu_page;
	$sc_menu_page = add_menu_page(
        'Style Customizer',
        'Style Customizer',
        'manage_options',
        'sc-customizer',
        'sc_customizer_main_page',
		''
    );
	$sc_submenu_page = add_submenu_page(
        'sc-customizer',
        'Add Tag Elements',
        'Add Tag Elements',
        'manage_options',
        'sc-add-element',
        'sc_add_element_callbk'
	);
}
add_action( 'admin_menu', 'sc_register_main_page' );

/*
* Callback function for loading main plugin page.
*/
function sc_customizer_main_page(){
	include('admin/main-page.php');
}

/*
* Callback function for loading sub menu page.
*/
function sc_add_element_callbk(){
	include('admin/add-element.php');
}

/*
* Load Required javascript needed for sub menu page.
*/
function sc_load_submenu_js( $hook ){
	global $sc_submenu_page;
	// Check is submenu page is loaded then only add custom js
	if( $sc_submenu_page != $hook  )
		return;
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'sc-jquery-ui', "https://code.jquery.com/ui/1.12.1/jquery-ui.js", array( 'jquery' ) );
	wp_enqueue_script( 'sc-submenu-js', plugins_url( 'js/custom.js' , __FILE__ ), array( 'jquery','sc-jquery-ui' ) );
	$array = array(
	'ajax_url' => admin_url('admin-ajax.php')
	);
	wp_localize_script( 'sc-submenu-js', 'data', $array);
}
add_action( 'admin_enqueue_scripts','sc_load_submenu_js' );

/*
* Load Required javascript needed for main menu page( style customizer ).
*/
function sc_load_mainmenu_js( $hook ){
	global $sc_menu_page;
	if( $sc_menu_page != $hook )
		return;
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'sc-menu-js', plugins_url( 'js/style-customizer.js' , __FILE__ ), array( 'jquery' ) );
	$array = array(
	'ajax_url' => admin_url('admin-ajax.php')
	);
	wp_localize_script( 'sc-menu-js', 'data', $array);
	
}
add_action( 'admin_enqueue_scripts','sc_load_mainmenu_js' );

/*
* Ajax call back for adding tag elements in database.
*/
function add_sc_element_callbk(){
	if( isset( $_POST ) ){
		global $wpdb;
		$element_val = $_POST['element_val'];
		$table_name = $wpdb->prefix . 'tag_elements';
		$sql = "Select tag from $table_name where tag = '$element_val'";
		$results = $wpdb->get_row( $sql );
		if( empty( $results ) ){
			$insert = $wpdb->insert( 
				$table_name, 
				array( 
					'tag' => $element_val, 
				) 
			);
			echo 1;
		}else{
			echo "exists";
		}
	}
	die;
}
add_action('wp_ajax_add_sc_element','add_sc_element_callbk');

/*
* Callback function which gets the tag data from database on change of tag element in backend
*/
function sc_get_elem_data_callbk(){
	if( isset( $_POST ) ){
		global $wpdb;
		$tag_val = $_POST['tag_elem'];
		$table_name = $wpdb->prefix . 'tag_elements';
		$sql = "Select style_type, style_data from $table_name where id = '$tag_val'";
		$results = $wpdb->get_row( $sql,ARRAY_A );
		$style_data = array(
		'style_type' => $results['style_type'],
		'style_data' => $results['style_data']
		);
		echo json_encode($style_data);
	}
	die;
}
add_action('wp_ajax_sc_get_elem_data','sc_get_elem_data_callbk');

/*
* Function returns all available tags in database as an object array.
*/
function sc_list_tag_elements(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'tag_elements';
	$results = $wpdb->get_results("select * from $table_name");
	if(!empty($results)){
		return $results;
	}else{
		return false;
	}
}

/*
* This function updates tag data in custom table provided posted values as a paramater. 
*/
function sc_update_tag_data( $element, $type, $tag_data ){
	global $wpdb;
	$table_name = $wpdb->prefix . 'tag_elements';
	$sql = "Select style_type, style_data from $table_name where tag = '$element'";
	$row = $wpdb->get_row($sql);
	$result = $wpdb->update($table_name,array('style_type'=>$type,'style_data'=>$tag_data),array('id'=>$element),array( '%s','%s' ));
	if($result){
		return true;
	}else{
		return false;
	}
}

/*
* Ajax Callback to delete external CSS file
*/
function sc_delete_css(){
	if(isset($_POST)){
		$css_file = $_POST['css_file'];
		$upload_dir = wp_upload_dir();
		$filepath = $upload_dir['basedir'].'/sc-uploads/'.$css_file;
		$isdel = unlink($filepath);
		return ($isdel ? true : false);
	}
	die;
}
add_action('wp_ajax_sc_delete_css','sc_delete_css');

function sc_upload_dir($upload) {
  $upload['subdir'] = '/sc-uploads';
  $upload['path']   = $upload['basedir'] . $upload['subdir'];
  $upload['url']    = $upload['baseurl'] . $upload['subdir'];
  return $upload;
}


function sc_enqueue_css_front_internal(){
	$upload_dir = wp_upload_dir();
	$dirpath = $upload_dir['basedir'].'/sc-uploads';
	$dirurl = $upload_dir['baseurl'].'/sc-uploads';
	
	if(file_exists($dirpath)){
		$files = array();

    foreach (scandir($dirpath) as $file) {
        if ('.' === $file) continue;
        if ('..' === $file) continue;

        $files[] = $file;
    }
	
	if(!empty($files)){
		$i=0;
		foreach($files as $file){
			$handle = "sc-css-$i";
			$path = $dirurl.'/'.$file;
			wp_enqueue_style($handle,$path);
			$i++;
		}
	}
	}	
}
add_action('wp_enqueue_scripts','sc_enqueue_css_front_internal');

/*
* Apply CSS if inline mode is selected for any tag.
*/
function sc_enqueue_css_front_inline() {
	wp_enqueue_style(
		'sc-inline-style',
		plugins_url('frontend/css/custom.css',__FILE__)
	);
	global $wpdb;
	$table_name = $wpdb->prefix . 'tag_elements';
	$sql = "select tag, style_type, style_data from $table_name where style_type='inline'";
	$results = $wpdb->get_results($sql);
	
	if(!empty($results)){
		$custom_css = '';
		foreach($results as $result){
			$data = wp_kses( $result->style_data, array( '\'', '\"' ));
		$custom_css .= "
					$result->tag{
					 $data
					}";
		}
		wp_add_inline_style( 'sc-inline-style', $custom_css );
	} 
	
}
add_action( 'wp_enqueue_scripts', 'sc_enqueue_css_front_inline' );

/*
* Apply CSS on page if internal mode is selected for any tag.
*/
function sc_apply_css(){
	global $wpdb;
	$table_name = $wpdb->prefix . 'tag_elements';
	$sql = "select tag, style_type, style_data from $table_name where style_type='internal'";
	$results = $wpdb->get_results($sql);
	if(!empty($results)){
		?><style type="text/css"><?php
		foreach($results as $result){
			echo $result->tag.'{';
			echo wp_kses( $result->style_data, array( '\'', '\"' ) );
			echo '}';
		}
		?>
		</style>
		<?php
	}
}
add_action('wp_footer','sc_apply_css');
?>