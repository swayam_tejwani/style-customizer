jQuery(document).ready(function(){
	
    var availableTags = [
      "h1",
      "h2",
      "h3",
      "h4",
      "h5",
      "h6",
      "p",
      "span",
      "label",
      "ul",
      "li",
      "input",
      "img",
      "ol",
      "a",
      "table",
      "thead",
      "tbody",
      "tfoot",
      "div",
      "section",
      "article",
	  "aside"
    ];
	
    jQuery( "#add_tag" ).autocomplete({
      source: availableTags
    });
	
	jQuery('#btn_add_tag').click(function(){
		var elem_val = jQuery('#add_tag').val();
		if(elem_val === ""){
			alert("Please enter tag element");
		}else{
			 jQuery('#btn_add_tag').prop('disabled', true);
			jQuery.ajax({
				url:data.ajax_url,
				type:'POST',
				data:{
					action:'add_sc_element',
					element_val:elem_val
				},
				success:function(data){
					jQuery('#add_element_form')[0].reset();
					jQuery('#btn_add_tag').prop('disabled', false);
					if(data == "exists"){
						alert("Tag already exists, please enter another value");
					}else{
						alert("Tag Added Successfully");
						
					}
				}
			});
		}
		
	});
  
})