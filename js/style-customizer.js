jQuery(document).ready(function(){
	jQuery('#select_tag').on('change',function(){
		var tag_elem = jQuery(this).val();
		if(tag_elem){
			jQuery.ajax({
				url:data.ajax_url,
				type:'POST',
				dataType: "JSON",
				data:{
					action:'sc_get_elem_data',
					tag_elem:tag_elem
				},
				success:function(jsonStr){
					//var obj = JSON.parse(jsonStr);
					if(jsonStr.style_type != null){
						jQuery('.sc_'+jsonStr.style_type).attr('checked', 'checked');
					}else{
						jQuery('.select_type').removeAttr('checked');
					}
					if(jsonStr.style_data != null){
						jQuery('#fill_css').val(jsonStr.style_data);
					}else{
						jQuery('#fill_css').val('');
					}
				}
			})	
		}
	})	
	
	jQuery('.del_css').click(function(e){
		var delid = jQuery(this).attr("id");
		e.preventDefault();
		
		if(confirm("Are you sure you want to delete this css file")){
			jQuery.ajax({
			url:data.ajax_url,
			type:'POST',
			data:{
				action:'sc_delete_css',
				css_file:delid
			},
			success:function(response){
				if(response){
					alert("file deleted successfully");
					location.reload();
				}else{
					alert("failed");
				}
			}
		});
		}
		
	});
	
})